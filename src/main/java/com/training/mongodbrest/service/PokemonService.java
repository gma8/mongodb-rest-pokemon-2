package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PokemonService implements PokemonServiceInterface {

    @Autowired
    private PokemonRepository pokemonRepository;

    public List<Pokemon> findAll() {
        return pokemonRepository.findAll();
    }

    public Optional<Pokemon> findById(String id) {
        return pokemonRepository.findById(id);
    }

    public Pokemon save(Pokemon pokemon) {
        // check there is a name in the pokemon, if not, don't save!!

        return pokemonRepository.save(pokemon);
    }

    public void delete(String id) {
        pokemonRepository.deleteById(id);
    }
}
