package com.training.mongodbrest.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokemonTest {

    @BeforeEach
    public void runBeforeEachTest() {
        System.out.println("this runs first!!");
    }


    @Test
    public void testPokemonGettersSetters() {
        Pokemon testPokemon = new Pokemon();
        testPokemon.setId("12345");
        testPokemon.setName("Pikachu");
        testPokemon.setType("Electric");

        assertEquals("12345", testPokemon.getId());
        assertEquals("Pikachu", testPokemon.getName());
    }

}
