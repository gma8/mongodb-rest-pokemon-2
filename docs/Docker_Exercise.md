## Create and Test a Dockerfile for a spring boot app

Add a Dockerfile to a spring boot project.

The Dockerfile should contain the following details:

* Build on top of the openjdk:11.0.12-jre-slim image

* Copy the jar that maven produces into the Docker image

* Expose port 8080

* The entry point should run java -jar <name of the jar in the docker image>

An example of the Dockerfile is below, but you may need to tweak the jar name to suit your specific app.
Look inside the target directory on your dev machine to get the actual jar name.

#### Example Dockerfile

```Dockerfile

FROM openjdk:11.0.12-jre-slim

COPY target/mongodb-rest-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]

```


### General steps to test the Dockerfile

1. Clone the repository (that already has a Dockerfile in it) onto a machine that has a jdk 11

2. cd into the cloned directory, make maven executable, then build the jar:
    cd <the cloned repo>
    chmod a+x mvnw
    ./mvnw clean package
    
3. If a jar is produced in the target/ directory you can now build the docker image:
    docker build -t <your docker tag name> .

4. Test the docker image by running it - you may also need to create a network so that you can run a mongo container

    ```bash
    docker network create mongo-net
    
    docker run -d --name mongo-container --network mongo-net mongo
    
    docker run --name java-app --network mongo-net -e DB_HOST=mongo-container -p 8081:8080 <your docker tag name>
    ```
    
5. You could go further by:
    a) tag the image with your docker hub name, and push to docker hub

    b) now try to run mongo and your image from docker hub on a play with docker machine


### Install java 11 on Amazon Linux

```sudo amazon-linux-extras install java-openjdk11```
(enter y if asked to confirm)

```sudo alternatives --config java```
(enter the number that corresponds to the new java)